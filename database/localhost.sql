-- phpMyAdmin SQL Dump
-- version 2.11.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2016 at 11:27 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `star_can_care`
--
CREATE DATABASE `star_can_care` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `star_can_care`;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `empid` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `dob` varchar(10) NOT NULL,
  `designation` varchar(30) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY  (`empid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`empid`, `name`, `address`, `dob`, `designation`, `contact`, `username`) VALUES
(1, 'ArunBabu', 'aaaaa', '05/09/1990', 'Software', 99977777, '476N3');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `lid` int(11) NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `confirmpass` varchar(30) NOT NULL,
  `category` varchar(7) NOT NULL,
  PRIMARY KEY  (`lid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`lid`, `username`, `password`, `confirmpass`, `category`) VALUES
(1, 'admin', 'admin', '', 'admin'),
(4, '476N3', '123', '123', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `userform`
--

CREATE TABLE `userform` (
  `fid` int(11) NOT NULL auto_increment,
  `district` varchar(30) NOT NULL,
  `panchayath` varchar(30) NOT NULL,
  `ward` int(11) NOT NULL,
  `kudumbanadhan` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `houseno` int(11) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `v1` varchar(10) NOT NULL,
  `v1name` varchar(30) NOT NULL,
  `v1age` int(11) NOT NULL,
  `v2` varchar(10) NOT NULL,
  `v2name` varchar(30) NOT NULL,
  `v2age` int(11) NOT NULL,
  `v3` varchar(10) NOT NULL,
  `v3name` varchar(30) NOT NULL,
  `v3age` int(11) NOT NULL,
  `v4` varchar(10) NOT NULL,
  `v4name` varchar(30) NOT NULL,
  `v4age` int(11) NOT NULL,
  `v5` varchar(10) NOT NULL,
  `v5name` varchar(30) NOT NULL,
  `v5age` int(11) NOT NULL,
  `v6` varchar(10) NOT NULL,
  `v6name` varchar(30) NOT NULL,
  `v6age` int(11) NOT NULL,
  `b1` varchar(10) NOT NULL,
  `b1name` varchar(30) NOT NULL,
  `b1age` int(11) NOT NULL,
  `b2` varchar(10) NOT NULL,
  `b2name` varchar(30) NOT NULL,
  `b2age` int(11) NOT NULL,
  `b3` varchar(10) NOT NULL,
  `b3name` varchar(30) NOT NULL,
  `b3age` int(11) NOT NULL,
  `u1` varchar(10) NOT NULL,
  `u1name` varchar(30) NOT NULL,
  `u1age` int(11) NOT NULL,
  `u2` varchar(10) NOT NULL,
  `u2name` varchar(30) NOT NULL,
  `u2age` int(11) NOT NULL,
  `u3` varchar(10) NOT NULL,
  `u3name` varchar(30) NOT NULL,
  `u3age` int(11) NOT NULL,
  `u4` varchar(10) NOT NULL,
  `u4name` varchar(30) NOT NULL,
  `u4age` int(11) NOT NULL,
  `other` text NOT NULL,
  `username` varchar(10) NOT NULL,
  `ucontact` bigint(20) NOT NULL,
  `date` varchar(10) NOT NULL,
  PRIMARY KEY  (`fid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `userform`
--

INSERT INTO `userform` (`fid`, `district`, `panchayath`, `ward`, `kudumbanadhan`, `address`, `houseno`, `contact`, `v1`, `v1name`, `v1age`, `v2`, `v2name`, `v2age`, `v3`, `v3name`, `v3age`, `v4`, `v4name`, `v4age`, `v5`, `v5name`, `v5age`, `v6`, `v6name`, `v6age`, `b1`, `b1name`, `b1age`, `b2`, `b2name`, `b2age`, `b3`, `b3name`, `b3age`, `u1`, `u1name`, `u1age`, `u2`, `u2name`, `u2age`, `u3`, `u3name`, `u3age`, `u4`, `u4name`, `u4age`, `other`, `username`, `ucontact`, `date`) VALUES
(1, 'tvm', 'vnjd', 0, 'sasi', 'sasiiiii', 123, 1234567890, 'Yes', 'aaa', 11, 'Yes', 'bb', 22, 'Yes', 'cc', 33, 'Yes', 'dd', 44, 'Yes', 'ee', 55, 'Yes', 'ff', 66, 'Yes', 'gg', 77, 'Yes', 'hh', 88, 'Yes', 'ii', 99, 'Yes', 'kkk', 12, 'Yes', 'lll', 34, 'Yes', 'mm', 43, 'Yes', 'nn', 45, 'erdyhrtuy', '476N3', 252534636, '06/30/2016'),
(2, 'Thiruvanathapuram', 'Kizhuvilam', 8, 'Ashokan', 'Ashokan', 155, 9895880512, 'No', '', 0, 'Yes', 'Anu', 25, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'No', '', 0, 'dgdfhyfgh', '476N3', 99956321, '06/29/2016');
